package main;

public class Question2 {

    public static void main(String[] args) {
        
    	int output = solution(15);
    	System.out.println("output = " + output);    	
    }
    
    public static int solution(int input) {
        
    	if(input < 1){
    		return 0;
    	}
    	
    	int output = 0;
    	
    	for(int i=1; i<=input; i++){
    		
    		if(i%15 == 0){
    			output ++;
    		}else if(i%3 == 0){
    			continue;
    		}else if(i%5 == 0){
    			continue;
    		}else{
    			output ++;
    		}
    		
    	} //end of for(int i=0; i<input; i++){    	
    	return output;    	
    } //end of public static int solution(int input) {
    
    
}
