package main;

import java.util.ArrayList;
import java.util.List;


public class Question1 {
	
    public static void main(String[] args) {
    	
        List<Object> a = new ArrayList<>();

        ArrayList<Integer> a1 = new ArrayList<Integer>();
        ArrayList<Integer> a2 = new ArrayList<Integer>();
        ArrayList<Integer> a3 = new ArrayList<Integer>();
        
        a1.add(1);
        a1.add(2);
        a1.add(3);
        a2.add(4);
        a2.add(5);
        a2.add(6);
        a3.add(7);
        a3.add(8);
        a3.add(9);

        a.add(a1);
        a.add(a2);
        a.add(a3);
        a.add("");
        a.add(10);
        
        System.out.println("Before a = " + a);
        reverse(a);
        System.out.println("After a = " + a);
    }  //end of public static void main(String[] args) {
    
    @SuppressWarnings("unchecked")
	public static ArrayList<Object> reverse(List<Object> list) {
    	
        if(list.size() >= 1) {
        	
            Object obj = list.remove(0);
            reverse(list);
            
            if(obj instanceof List){
            	list.add(reverse((List<Object>) obj));
            }else{            
            	list.add(obj);
            }
        }
        return (ArrayList<Object>) list;
    } //end of public static ArrayList<Object> reverse(List<Object> list) {
    
} //end of public class Question1 {


